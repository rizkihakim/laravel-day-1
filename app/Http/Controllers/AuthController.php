<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
      return view('register');
    }

    public function sapa(Request $request) {
      return view('welcome');
    }

    public function sapa_post(Request $request) {
      $first_name = $request["first_name"];
      $last_name = $request["last_name"];
      return view('welcome', ['n1' => $first_name, 'n2' => $last_name]);
    }
}
