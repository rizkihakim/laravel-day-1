<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Buat Account Baru</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form class="" action="/welcome" method="post">
      @csrf
      <label>First Name:</label><br><br>
      <input type="text" name="first_name" value=""><br><br>

      <label>Last Name:</label><br><br>
      <input type="text" name="last_name" value=""><br><br>

      <label>Gender:</label> <br><br>
      <input type="radio" name="gender" value="0" checked>Male <br>
      <input type="radio" name="gender" value="1">Female <br>
      <input type="radio" name="gender" value="2">Other <br><br>

      <label>Nationality:</label> <br><br>
      <select>
        <option value="indonesian">Indonesian</option>
        <option value="foreigner">Foreigner</option>
      </select> <br><br>

      <label>Language Spoken:</label> <br><br>
      <input type="checkbox" name="language" value="indonesia">Bahasa Indonesia <br>
      <input type="checkbox" name="language" value="english">English <br>
      <input type="checkbox" name="language" value="other">Other:
      <input type="text" name="language" placeholder="chinese, japanese, etc"> <br><br>

      <label>Bio:</label><br><br>
      <textarea name="name" rows="10" cols="30"></textarea> <br>
      <input type="submit" name="" value="Sign Up">
    </form>
  </body>
</html>
